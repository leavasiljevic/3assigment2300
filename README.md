# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Smashing Pixels offers web services and simple tutorials that allow a user to improve their online presence. 
To provide a better service to Smashing Pixels’ customers, a search engine optimization (SEO) tool will be developed 
to improve online presence. The tool will allow companies to determine which components of their website could be 
improved by comparing their own page with a known competitor or other similar companies whose web page ranks higher 
on a search engine results page (SERP). Existing SEO tools can be challenging to use and interpret for smaller 
companies with beginner web development knowledge and skill sets. The new web tool must be “simple and smart”: 
allowing small companies wishing to improve their online visibility to easily navigate the tool, enter information,
and interpret the data analyzed


### How do I get set up? ###
Install Node.js
To get started, first we will need to install Node.js which is a run-time environment for JavaScript (👉 in english: Node will help you execute JavaScript code).

Many developers get excited when talking about Node. Before only web browsers like Google Chrome had a JavaScript engine that could read and display code written in JavaScript.
For Chrome this interpreter is called V8. The new feature that made Node so popular is that it allows JavaScript to run basically on all machines — which means the 
browser is no longer a restriction for the execution of JavaScript.

It’s save to say that Node is the best choice when building a simple server for all kind of web apps. 
So let’s install it. I will tell you two ways, one quick way to install and another option that’s a bit more complex at first but later on much more convenient.

**(1) Quick way to install Node.js**
Go to the official page of Node.js and download the install package for your operating system. Use the LTS version not the current one.
After the download is complete install the package like any other app on your Mac or PC
Next you can go to your Terminal program of choice. In case you don’t have a Terminal app like iTerm2 or Hyper installed, simply open the Terminal that comes pre-installed on every Mac. 
In case you are a Windows user check here how to use terminal on Windows and don’t go crazy.
You can type the following command into your Terminal to see if everything was installed correctly: $ node -v. If it works fine you should see a Node version number now. 
Also check if npm was installed with $ npm -v. Npm is the Node Package Manager that comes with Node when being installed. We will use it in the next steps to install Express and start our virtual server.

**(2) Better way to install Node.js**
Instead of the above described way I prefer using Homebrew which is a package manager for macOS. It allows you to install missing apps super fast via the Terminal. 
Windows users must take another package manager like Scoop instead. They are pretty similar and for demonstration purposes I will go with Homebrew and show you how to install Node via Homebrew.

Again you can go to the Terminal and paste the following promt (without the $-sign) in there. In case you’re wondering: it simply checks the GitHub repository from Homebrew and installs the app from there.
$ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
If Homebrew was installed correctly, we will be ready now to install Node with this simple command: $ brew install node
You can check if everything is looking good by typing in: $ node -v and $ npm -v (which should give you the version number of your installed Node).
But why make this effort to install Node via a package manager like Homebrew? There are several reasons this is a good idea:

If you are using Node’s install manager it is possible that you run into access issues that require you to make changes in your system using a command called $ sudo.
Also if you ever want to uninstall without Node this will be very messy as you need to track all the files that were created.
Lastly, also it’s much easier to keep your Node version up-to-date when using Homebrew.
Setup your first app
You’re still with me, right? Great, so let’s finally go on and build an actual web app and local server!

To do this quite conveniently we can use the express-generator which is a great command-line tool that creates an application skeleton for us. Otherwise you
would be required to write more advanced code like setting up a server instance, configuring a view engine, etc. Although this is great to know it will 
not be necessary to run your first app on a web server.

Express generator is straightforward. Simply take the following command and hack it into your terminal: $ npm install express-generator -g. With the -g 
we install Express globally which means that you can access the package from any directory.

While still on the Terminal you can now create a new app with express-generator by typing: $ express -v ejs -c sass myapp. In this example myapp 
will be the name of your project. And guess what? You have just build your first app! To check into the myapp directory that we’ve just created you can type $ cd myapp.

**Admire the app you’ve just build**
Take a look at the myapp project that you have just created. To see your files in the code editor just use this line: $ code . while still being in the myapp folder on your Terminal.

For this to work you must of course have installed a code editor like Visual Studio Code or Atom.

When opening the editor you can see the project and all the files that were automatically created for you with Express generator. 
Within the index.ejs you can make edits and build your complex web app from there. For now let’s just leave it as is and continue to build our server.


**Setup of express-generator within the code editor**
We are almost there.

Last thing: we must install various additional third-party packages (which are listed as dependencies in the package.json file). 
These are commonly required by Express to run the server as you would expect it. Good news is that this will be pretty easy as you can install all of these via npm at once. 
Open up your Terminal and use this prompt:

$ npm install. You can check if your installation was successful by going into your code editor again. 
You will see a new folder called node_modules like in my example above (hint: exclude this in case your uploading to GitHub).

**Start your app on a virtual web server**
Finally let us run our app on a web server. And the very most of what’s necessary was already done in prevoius steps! Two simple steps and you are there:

While in the Terminal prompt this command: $ npm start. This will start a virtual server.
Go to the address bar of your internet browser and type localhost:3000. Localhost is a top-level-domain (TLD) just like .com or .org. 
However, it’s reserved for documentation and testing purpose. With :3000 you call the default port to access the newly build server.

**Welcome screen on your local server once the Express app is running**
Where to go from here
Congrats! You have created your first app and actually run it on your own server. From here you could start building your custom app. 
The app skeleton is already setup in a way that allows you to build your site within the index.ejs. 
In case you like to build anything more advanced than a simple site you should consider using partials.
It means that you build your app in components that you can reference from your index.ejs.
Conveniently, we have already installed the view engine EJS that will help you while building specific parts of your app in components.

**We put our version one on Heroku**

**About**
This repository contains HTML5 and CSS3 code for a college project about the author Haruki Murakami. It contains examples of HTML and CSS formatting
for tables, forms, and navigation bars.

**How to use this repository**
To use this project, simply clone the repository following Atlassian guidelines (found here: https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html). 

To run the index.html on a browser, right click on the index.html and chose Open With and click the browser of your choice. 

To edit or view the contents of any file, right click and open in a text editor - I recommend Notepad ++. 

**License**
This project is licensed under the MIT Open Source License. The MIT License is the most popular open source licenses. It allows developpers to use software under
the condition that the original developper is acknowledged (the original copyright must be included), the project remains open source, and that the user
understands that the original developper holds no liability for any pitfalls of the code.


**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. 
If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, 
see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree]
(https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).



