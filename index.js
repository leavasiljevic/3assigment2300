import express from 'express';
import bodyParser  from "body-parser";
const fetch = require('node-fetch');
const cheerio = require('cheerio');

// Create a new express application instance
const app = express();

app.use(bodyParser.urlencoded({extended:true}));
app.use(express.static("www"));

app.get("/users/:uname", (req, res) => {
    res.end("Hello " + req.params.uname);
});

function oneSite(sBody){
    const $ = cheerio.load(sBody);
    var aResult = [];
    var nAnchorTags= $("a").length;
    var nBoldTag=$("b").length;
    var nItalicTag=$("i").length;
    var nImgWithoutAlt= $('img:not([alt])').length;
    var sTitle=$("title").html();
    var sResult="";
    sResult= sTitle + " has " + nAnchorTags + " anchors" + " and " + nImgWithoutAlt + " images without \"alt\" tags," +
     " and " + nBoldTag + " bold tags, and " + nItalicTag + " italic tags";
    return sResult;
}

app.get("/compare/", (req, res) => {
    let sUrl = req.query.q;
    let sCompetitor = req.query.q1;
    fetch(sUrl)
    .then(res => res.text())
    .then(body => {
        var sResult = oneSite(body);
        fetch(sCompetitor)
        .then(res => res.text())
        .then(body => {
            sResult += "<br/>"+ oneSite(body);
            res.end(sResult);
        }); 
        
    }); 
});




var port = process.env.PORT || parseInt(process.argv.pop()) || 3000;

app.listen(port, () => console.log('Example app listening on port ' + port + '!'));
 